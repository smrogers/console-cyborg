define(function () {
	'use strict';

	var LEVELS = {
		ALL:    65535,
		NONE:       0,
		ERROR:      1,
		WARN:       2,
		TODO:       4,
		LOG:        8,
		INFO:      16,
		DEBUG:     32,
		METHOD:    64
	};

	var STYLE = {};
	STYLE[LEVELS.ERROR]  = 'background: red; color: white';
	STYLE[LEVELS.WARN]   = 'background: yellow';
	STYLE[LEVELS.TODO]   = 'background: #CF5214; color: white';
	STYLE[LEVELS.LOG]    = 'color: green';
	STYLE[LEVELS.INFO]   = 'color: #AAA';
	STYLE[LEVELS.DEBUG]  = false;
	STYLE[LEVELS.METHOD] = 'color: #AAF';

	var LEVEL = {
		error: 			LEVELS.ERROR,
		exception:      LEVELS.ERROR, // alias of error
		warn: 			LEVELS.WARN,
		log: 			LEVELS.LOG,
		info: 			LEVELS.INFO,
		debug: 			LEVELS.DEBUG,
		assert:         LEVELS.ALL,
		clear:          LEVELS.ALL,
		count:          LEVELS.LOG,
		dir:            LEVELS.LOG,
		dirxml:         LEVELS.LOG,
		group:          LEVELS.LOG,
		groupCollapsed: LEVELS.LOG,
		groupEnd:       LEVELS.ALL,
		markTimeline:   LEVELS.DEBUG,
		profile:        LEVELS.DEBUG,
		profileEnd:     LEVELS.DEBUG,
		table: 			LEVELS.LOG,
		time:           LEVELS.LOG,
		timeEnd:        LEVELS.LOG,
		timeline:       LEVELS.DEBUG,
		timelineEnd:    LEVELS.DEBUG,
		timeStamp:      LEVELS.LOG,
		trace:          LEVELS.DEBUG,

		// custom methods
		method:         LEVELS.METHOD,
		todo:           LEVELS.TODO
	};

	var NOOP = function () {};

	if (!window.console) {
		window.console = {};
	}

	var _console = window.console;

	function Cyborg (prefix) {
		this._prefix = prefix || false;
		this._install();
	}

	Cyborg.prototype = {
		LEVEL: LEVELS,

		_prefix: false,

		_check: function (level) {
			var globalLevel = this.level();

			if (globalLevel === LEVELS.NONE) { return false; }
			if (globalLevel === LEVELS.ALL) { return true; }

			return globalLevel & ~~level;
		},

		_install: function () {
			/*jshint forin:false, loopfunc:true*/

			var self = this;
			var constructor = function (key) {
				self[key] = function () {
					if (!self._check(LEVEL[key])) { return; }
					_console[key].apply(_console, arguments);
				};
			};

			for (var key in LEVEL) {
				// install noop if original doesn't have
				// this method (shitty browsers, etc)
				if (!_console[key]) {
					_console[key] = NOOP;
					continue;
				}

				// do nothing if cyborg varient exists
				var cyborgVarient = this[key];
				if (cyborgVarient) { continue; }

				// install a passthrough function that checks for level
				constructor(key);
			}
		},

		_process: function (args, argIndexToModify, level, originalMethod) {
			if (!this._check(level)) { return; }

			args = Array.prototype.slice.call(args);

			var argToModify = args[argIndexToModify];
			var hadColorCode = false;

		    // do we prepend to string or shift new first argument
		    if (this._prefix !== false) {
				if (!(typeof argToModify === 'string' || argToModify instanceof String)) {
					args.unshift(this._prefix);

					argToModify = this._prefix;
				} else {
					hadColorCode = (argToModify.match(/^\%c/i) || []).length;

					// (1) remove existing %c
					// (2) prefix
					// (3) update first argument

					if (hadColorCode) {
						argToModify = argToModify.replace(/^\%c\s*/, this._prefix + ' ');
					} else {
						argToModify = this._prefix + ' ' + argToModify;
					}

					args[argIndexToModify] = argToModify;
				}
			}

			// style
			var style = STYLE[level] || false;

			// override or splice in style argument
			if (style) {

				// (1) add %c and prefix
				// (s) override first argument
				argToModify = '%c' + argToModify;
				args[argIndexToModify] = argToModify;

				if (hadColorCode) {
					args[argIndexToModify + 1] = style;
				} else {
					args.splice(argIndexToModify + 1, 0, style);
				}
			}

			//
			originalMethod.apply(_console, args);
		},

		error:     function () { this._process(arguments, 0, LEVEL.error,  _console.error); },
		exception: function () { this._process(arguments, 0, LEVEL.error,  _console.error); },
		warn:      function () { this._process(arguments, 0, LEVEL.warn,   _console.warn); },
		log:       function () { this._process(arguments, 0, LEVEL.log,    _console.log); },
		info:      function () { this._process(arguments, 0, LEVEL.info,   _console.info); },
		debug:     function () { this._process(arguments, 0, LEVEL.debug,  _console.debug); },
		method:    function () { this._process(arguments, 0, LEVEL.method, _console.log); },
		todo:      function () { this._process(arguments, 0, LEVEL.todo,   _console.log); },
		assert:    function () { this._process(arguments, 1, LEVEL.assert, _console.assert); },

		time: function () {
			// Chrome will deep-match objects given to time/timeEnd.
			// In order to be able to prefix (a more desirable feature)
			// we are effectively neutering the deep-matching by rendering
			// the first argument to a string.

			var argument = (this._prefix) ? this._prefix + ' ' + String(arguments[0]) : String(arguments[0]);
			_console.time.call(_console, argument);
		},

		timeEnd: function () {
			var argument = (this._prefix) ? this._prefix + ' ' + String(arguments[0]) : String(arguments[0]);
			_console.timeEnd.call(_console, argument);
		},

		level: function () {
			var level = ~~window.__CYBORG_CONSOLE_LOG_LEVEL__;
			if (level === 0) { return level; }
			return level || LEVELS.ALL;
		},

		setLevel: function (level) {
			window.__CYBORG_CONSOLE_LOG_LEVEL__ = ~~level;
		}
	};

	return Cyborg;
});
