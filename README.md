A light-weight set of enhancements for window.console.

- Ensures all browsers have a minimum set of methods shared by common console dev tools.
    - Missing methods are set to a no-op function.
- Add simple prefix string to augmented methods.
- Gives uniform styling to error, info, and warn.
- Disable certain commands by setting a global level variable.
- Methods that aren't augmented are passed through to the original console.

# Bitwise Muting (How to disable certain log methods) #

Consider the following:

```
#!javascript
var LEVELS = {
	ALL:    65535,
	NONE:       0,
	ERROR:      1,
	WARN:       2,
	TODO:       4,
	LOG:        8,
	INFO:      16,
	DEBUG:     32,
	METHOD:    64
};
```

These are the levels assigned to console methods. 

If you wanted to mute debug methods:

```
#!javascript
var _console = new Cyborg();

var LEVEL = _console.LEVEL;
_console.setLevel(LEVEL.ALL ^ LEVEL.DEBUG);
_console.debug('test') // won't be logged
_console.log('test') // is logged
```

Want to only see debug messages?

```
#!javascript
_console.setLevel(LEVEL.DEBUG);
_console.debug('test') // yep
_console.log('test') // nope
```

Last one. How about just error and warn?
```
#!javascript
_console.setLevel(LEVEL.ERROR | LEVEL.WARN);
```

Keep in mind `ConsoleCyborg.setLevel` is a global setting.

# Superseding window.console #
Misleading header is misleading. Most browsers won't let you replace window.console unless it doesn't exist (as in the case of older browsers). But thanks to scope, we can finagle.

```
#!javascript
// abstractView.js
define(function (require) {
	'use strict';

	var console = window.console;

	// libraries
	var Backbone = require('backbone');

	//
	return Backbone.View.extend({
		initialize: function (options) {
			this.installConsole();
		},

		toString: function () {
			return '[AbstractView]';
		},

		installConsole: function () {
			console = new Cyborg(this.toString());
			this.__console(console);
		},

		__console: function (console__) {
			console = console__;
		},
	});
});

// subclassView.js
define(function (require) {
	'use strict';

	var console = window.console;

	// view
	var AbstractView = require('abstractView');

	//
	return AbstractView.extend({
		initialize: function (options) {
			this.installConsole();

			console.log('this is a test');
			// [SubclassView] this is a test!
		},

		toString: function () {
			return '[SubclassView]';
		},

		__console: function (console__) {
			console = console__;
		},
	});
});
```

While it is true manually adding `var console` and `__console` method to every class is tedious, not everything in the world can be automated. Then again, a good IDE has templates.


# Examples #
I have styled most of the common log methods to my own tastes, making them stand out (or not) as seemingly fit.

### No prefix ###

```
#!javascript
var noPrefix = new Cyborg();
noPrefix.error('error');
noPrefix.exception('exception'); // alias of error
noPrefix.warn('warn');
noPrefix.log('log');
noPrefix.info('info');
noPrefix.debug('debug');
noPrefix.method('method');
noPrefix.todo('todo');
noPrefix.time('time'); noPrefix.timeEnd('time');
noPrefix.assert(1 === 2, 'assert');
```

![Screen Shot 2015-01-07 at 17.10.00.png](https://bitbucket.org/repo/EyE68M/images/3761574164-Screen%20Shot%202015-01-07%20at%2017.10.00.png)

### Prefixed ###

```
#!javascript
var prefixed = new Cyborg('[PREFIX]');
prefixed.error('error');
prefixed.exception('exception');
prefixed.warn('warn');
prefixed.log('log');
prefixed.info('info');
prefixed.debug('debug');
prefixed.method('method');
prefixed.todo('todo');
prefixed.time('time');
prefixed.timeEnd('time');
prefixed.assert(1 === 2, 'assert');
```

![Screen Shot 2015-01-07 at 17.11.52.png](https://bitbucket.org/repo/EyE68M/images/2246545207-Screen%20Shot%202015-01-07%20at%2017.11.52.png)

### Real World Usage ###
Personally, I turn on timestamps in the dev tools, which is why I didn't add that functionality to this tool.

![Screen Shot 2015-01-07 at 17.25.42.png](https://bitbucket.org/repo/EyE68M/images/2114353011-Screen%20Shot%202015-01-07%20at%2017.25.42.png)